-- CAPSTONE

-- 1. Return the customerName of the customers who are from the Philippines
	SELECT customerName FROM customers WHERE country LIKE "Phil%";


-- 2. Return the contactLastName and contactFirstName of customers with name "La Rochelle Gifts"
	SELECT contactLastName, contactFirstName FROM customers WHERE customerName LIKE "La Rochelle Gifts";


-- 3. Return the product name and MSRP of the product named "The Titanic"
	SELECT productName, MSRP FROM products WHERE productName LIKE "The Titanic";


-- 4. Return the first and last name of the employee whose email is "jfirrelli@classicmodelcars.com"
	SELECT firstName, lastName FROM employees WHERE email LIKE "jfirrelli@classicmodelcars.com";


-- 5. Return the names of the customers who have no registered state
	SELECT customerName FROM customers WHERE state IS NULL;


-- 6. Return firstName, lastName, email of the employee whose last name is Patterson and firstName is Steve
	SELECT firstName, lastName, email FROM employees WHERE lastName LIKE "Patterson" AND firstName LIKE "Steve";


-- 7. Return customerName, country, and creditLimit of customers whose countries are NOT USA and whose credit limits are greater than 3000.
	SELECT customerName, country, creditLimit FROM customers WHERE country != "USA" AND creditLimit > 3000.00;  


-- 8. Return the customer numbers of orders whose comments contain the string 'DHL';
	SELECT customerNumber FROM orders WHERE comments LIKE "%DHL%";


-- 9. Return productLines whose text description mentions the phrase 'state of the art';
	SELECT productLine FROM productlines WHERE textDescription LIKE "%state_of_the_art%";


-- 10. Return the countries of customers without duplication
	SELECT DISTINCT country FROM customers;


-- 11. Return the statuses of orders without duplicaton
	SELECT DISTINCT status from orders;


-- 12. Return the customer names and countries of customers whose country is USA, France or Canada
	SELECT customerName, country FROM customers WHERE country = "USA" OR country = "France" OR country = "Canada";


-- 13. Return the firstName, lastName, and office's city of employees whose offices are in Tokyo
	SELECT firstName, lastName, city FROM employees
		JOIN offices ON employees.officeCode = offices.officeCode WHERE city = "Tokyo";


-- 14. Return customer names of customers who were served by the employee named "Leslie Thompson";
	SELECT customerName FROM customers
		JOIN employees ON customers.salesRepEmployeeNumber = employees.employeeNumber
		WHERE firstName = "Leslie" AND lastName = "Thompson";


-- 15. Return product names and customer name of products ordered by "Baane Mini Imports"
	SELECT productName, customerName FROM customers
		JOIN orders ON customers.customerNumber = orders.customerNumber
		JOIN orderdetails ON orders.orderNumber = orderdetails.orderNumber
		JOIN products ON orderdetails.productCode = products.productCode
		WHERE customerName = "Baane Mini Imports"; 


-- 16. Return employees first name, employees' last name, customers' names, and offices' countries of transactions whose customers and offices are in the same country.
	SELECT employees.firstName, employees.lastName, customers.customerName, offices.country FROM customers
		JOIN employees ON customers.salesRepEmployeeNumber = employees.employeeNumber
		JOIN offices ON employees.officeCode = offices.officeCode
		WHERE customers.country = offices.country;


-- 17. Return the productName and quantity in stock of products that belong to the product line "planes" with stock quantities less than 1000
	SELECT products.productName, products.quantityInStock FROM products
		WHERE productLine = "planes" AND  quantityInStock < 1000;


-- 18. Return the customer's name with a phone number containing "+81".
	SELECT customerName, phone FROM customers
		WHERE phone LIKE "+81%";


		

